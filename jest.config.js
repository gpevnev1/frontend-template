module.exports = {
  clearMocks: true,

  coverageDirectory: "coverage",

  coveragePathIgnorePatterns: [
    "/node_modules/"
  ],

  testEnvironment: "node",

  testMatch: [
    "**/tests/**/*.test.js"
  ]
};
