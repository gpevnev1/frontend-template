FROM node as dependencies

WORKDIR /build

COPY package.json /build/package.json

RUN npm install

FROM dependencies AS build

COPY ./webpack.config.js /build/webpack.config.js
COPY ./postcss.config.js /build/postcss.config.js
COPY ./.babelrc /build/.babelrc
COPY ./src/ /build/src/

RUN npm run build

FROM nginx:alpine

COPY --from=build /build/dist/ /usr/share/nginx/html