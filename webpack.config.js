const { EnvironmentPlugin, DefinePlugin } = require("webpack");
const MiniCssPlugin = require("mini-css-extract-plugin");
const HtmlPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const path = require("path");
const dotenv = require("dotenv");

const ENV = String(process.env.NODE_ENV || "development");

// Using default / provided environment-variables for development
dotenv.config({ path: ENV === "development" ? "./src/.env" : "./src/.env.prod" });

const APP = String(process.env.APP);
const BY = String(process.env.BY);
const SRC = "./src/index.js";
const DIST = "dist";
const STYLE_SHEET = "styles.css";

const miniCssPlugin = new MiniCssPlugin({
  filename: STYLE_SHEET
});

const htmlPlugin = new HtmlPlugin({
  title: APP,
  filename: "index.html",
  template: "./src/pages/index.html",
  minify: {
    collapseWhitespace: true,
    removeComments: true,
    removeRedundantAttributes: true,
    removeScriptTypeAttributes: true,
    removeStyleLinkTypeAttributes: true,
    useShortDoctype: true
  },
  favicon: "./src/assets/favicon.ico"
});

const cleanPlugin = new CleanWebpackPlugin({
  cleanOnceBeforeBuildPatterns: path.join(__dirname, DIST)
});

const assetsCopyPlugin = new CopyPlugin([
  { from: "./src/assets", to: "." } // Relative to Dist-Folder (Directly to)
]);

// Varaibles (Allows defining and substituting ANY Constant) - Allows using Variables without ANY Environment
const variablesPlugin = new DefinePlugin({
  // "process.env.APP": JSON.stringify(APP),
  "BY": JSON.stringify(BY)
});
// SUPER-IMPORTANT: COMPLETE Replacing (NOT AS STRINGS) -> Stringify to keep the quotes

// Environment (Prefixing ALL variables with "process.env") - Can only be accessed through Environment-Variables
const environmentPlugin = new EnvironmentPlugin({
  "APP": JSON.stringify(APP),
})

module.exports = {
  entry: {
    app: ["@babel/polyfill", SRC]
  },

  output: {
    path: path.join(__dirname, DIST),
    filename: "[name].js"
  },

  resolve: {
    extensions: [".js", ".jsx", ".scss"]
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: "babel-loader"
      },

      {
        test: /\.css$/,
        use: [MiniCssPlugin.loader, "css-loader", "postcss-loader"]
      },

      {
        test: /\.scss$/,
        use: [MiniCssPlugin.loader, "css-loader", "postcss-loader", "sass-loader"]
      },

      // Images used directly from CSS and JS
      // Imporant: Paths are changed automatically in JS / CSS / SCSS
      {
        test: /\.(png|jpe?g|tiff|gif)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "./images/[name].[ext]"
          }
        }
      }
    ]
  },

  plugins: [
    variablesPlugin,
    environmentPlugin,
    miniCssPlugin,
    htmlPlugin,
    ...(ENV == "production" ? [
      cleanPlugin,
      // Files loaded dynamically / separately from the content
      assetsCopyPlugin  // Copying Static Files and Assets to Dist-Folder only when building the application
    ] : [])
  ],

  target: "web",

  // Generating Source-Maps
  devtool: ENV == "production" ? false : "source-map", // No Source-Maps for Production-Builds

  devServer: {
    port: 8000,
    contentBase: "./src/assets",
    historyApiFallback: true,
  }
};