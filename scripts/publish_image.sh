#!/bin/bash

IMAGE_NAME="frontend-template"

if [ -z "$1" ]
then
  VERSION="latest"
else
  VERSION="$1"
fi

if [ -z "$DOCKER_ACCESS_TOKEN" ] || [ -z "$DOCKER_USERNAME" ]
then
  echo "Docker credentials not provided"
  exit 1
fi

REMOTE_IMAGE_NAME="$DOCKER_USERNAME/$IMAGE_NAME:$VERSION"

docker logout
echo "$DOCKER_ACCESS_TOKEN" | docker login --username=$DOCKER_USERNAME --password-stdin

docker image build -t $IMAGE_NAME .

docker image tag $IMAGE_NAME $REMOTE_IMAGE_NAME
docker push $REMOTE_IMAGE_NAME

