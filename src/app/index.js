import React from "react";
import { render } from "react-dom";
import App from "./ui/App";

render(
  <App />,
  document.getElementById("root"),
  () => {
    // Notes: SW, Initialization, Connections, etc.
    console.log(process.env.APP, BY);
  }
);